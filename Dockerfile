# Install the base requirements for the app.
# This stage is to support development.
FROM python:3.9.5

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY app ./
COPY herbDB.csv ./
COPY train_model.h5 ./
COPY class_to_num.json ./

CMD [ "python", "./server.py" ]
EXPOSE 25842/tcp
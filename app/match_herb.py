
import csv


def match_herb(classNo):
    with open('./herbDB.csv', newline='',encoding='utf8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if int(row['herb_No']) == classNo:
                return row['herb_name'],row['herb_desc']
    return 0,0
